
# Influxdb2 staff
https://github.com/ChristianLempa/boilerplates/blob/main/docker-compose/influxdb/docker-compose.yaml
https://www.smarthomebeginner.com/influxdb-docker-compose-guide/
*** OLD staff ***
https://www.influxdata.com/blog/getting-started-with-influxdb-2-0-scraping-metrics-running-telegraf-querying-data-and-writing-data/

# Zenoh staff
https://zenoh.io/docs/getting-started/quick-test/
https://zenoh.io/docs/manual/plugin-storage-manager/
https://github.com/eclipse-zenoh/zenoh-backend-influxdb
https://github.com/eclipse-zenoh/zenoh-backend-filesystem
https://github.com/eclipse-zenoh/zenoh-backend-s3
# AWS S3
https://zenoh.io/blog/2023-07-17-s3-backend/
https://docs.aws.amazon.com/AmazonS3/latest/userguide/example-walkthroughs-managing-access-example1.html
# MinIO (S3 compatible)
https://min.io/docs/minio/linux/reference/minio-server/settings.html

# TO READ:
https://zenoh.io/docs/apis/rest/
https://zenoh.io/docs/getting-started/deployment/
https://github.com/eclipse-zenoh/zenoh-python/tree/master/examples

# TO READ NEXT:
https://zenoh.io/blog/2022-11-29-zenoh-alignment/
https://zenoh.io/blog/2023-02-10-zenoh-flow/


# Read/Publish/Delete from/to Zenoh
curl http://localhost:8000/demo/example/test
curl -X PUT -H 'content-type:text/plain' -d '.....' http://localhost:8000/demo/example/test
curl -X DELETE http://localhost:8000/demo/example/test

# Send binary file  --  Use z_file_test.py to receive and save it
curl -X PUT -H 'content-type:image/png' -H 'Filename="an_image.png"' --data-binary  @zenoh-dragon-bg-150x163.png   http://zenoh1:8000/myhome/test/img

# Influxdb2 - send data to influx
curl -i -X POST 'http://localhost:8086/api/v2/write?bucket=zenoh_example&org=my-org' \
    --header 'Authorization: Token my-super-secret-auth-token' \
    --data-raw 'test6 value=6000'


# HELM -- Convert docker-compose.yml to charts
https://github.com/metal3d/katenary
https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/
