import zenoh, random, time

random.seed()

def read_temp():
    return random.randint(15, 30)

if __name__ == "__main__":
    """ session = zenoh.open(zenoh.Config.from_obj({
        'mode': 'client',
        'connect': {
            'endpoints': ["tcp/zenoh1:7447"]
        }
    })) """
    session = zenoh.open(zenoh.Config.from_file("zenoh-client.json5"))
    info = session.info()
    print(f"zid: {info.zid()}")
    print(f"routers: {info.routers_zid()}")
    print(f"peers: {info.peers_zid()}")

    key = 'myhome/kitchen/temp'
    pub = session.declare_publisher(key)
    while True:
        t = read_temp()
        buf = f"{t}"
        print(f"Putting Data ('{key}': '{buf}')...")
        pub.put(buf)
        time.sleep(1)