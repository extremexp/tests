import zenoh, time

if __name__ == "__main__":
    session = zenoh.open(zenoh.Config.from_file("zenoh-client.json5"))
    info = session.info()
    print(f"zid: {info.zid()}")
    print(f"routers: {info.routers_zid()}")
    print(f"peers: {info.peers_zid()}")

    #session = zenoh.open()
    replies = session.get('myhome/xyz/bin.png', zenoh.ListCollector())
    for reply in replies():
        try:
            print(f"Reply {reply.ok.kind} ('{reply.ok.key_expr}': '{reply.ok.encoding}')")
            print(f"Encoding {reply.ok.encoding}")
            file_name = f"file_{round(time.time() * 1000)}.png"
            with open(file_name, "wb") as binary_file:
                # Write bytes to file
                binary_file.write(reply.ok.payload)
            print(f"Saved in file: {file_name}")
        except:
            print("Received (ERROR: '{}')"
                .format(reply.err.payload))
session.close()