import zenoh, time

# *** PURPOSE ***
# This script waits for PNG image files in 'test/img', and writes them to files
#
# *** HOW TO SEND AN IMAGE FILE ***
# curl -X PUT \
#       -H 'content-type:image/png' \
#       -H 'Filename="an_image.png"' \              <---- Currently not used
#       --data-binary  @zenoh-dragon-bg-150x163.png \
#       http://zenoh1:8000/myhome/test/img

def listener(sample):
    #print(f"Type {type(sample.payload)}")
    #print(f"Type {sample}")
    print(f"Received {sample.kind} ('{sample.key_expr}': '{sample.encoding}')")
    print(f"Encoding {sample.encoding}")
    file_name = f"file_{round(time.time() * 1000)}.png"
    with open(file_name, "wb") as binary_file:
        # Write bytes to file
        binary_file.write(sample.payload)
    print(f"Saved in file: {file_name}")

if __name__ == "__main__":
    session = zenoh.open(zenoh.Config.from_file("zenoh-client.json5"))
    info = session.info()
    print(f"zid: {info.zid()}")
    print(f"routers: {info.routers_zid()}")
    print(f"peers: {info.peers_zid()}")

    #session = zenoh.open()
    sub = session.declare_subscriber('myhome/test/img', listener)
    time.sleep(60)