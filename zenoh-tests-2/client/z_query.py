import zenoh

if __name__ == "__main__":
    session = zenoh.open(zenoh.Config.from_file("zenoh-client.json5"))
    info = session.info()
    print(f"zid: {info.zid()}")
    print(f"routers: {info.routers_zid()}")
    print(f"peers: {info.peers_zid()}")

    #session = zenoh.open()
    replies = session.get('myhome/kitchen/temp', zenoh.ListCollector())
    for reply in replies():
        try:
            print("Received ('{}': '{}')"
                .format(reply.ok.key_expr, reply.ok.payload.decode("utf-8")))
        except:
            print("Received (ERROR: '{}')"
                .format(reply.err.payload.decode("utf-8")))
session.close()