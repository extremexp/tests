
# ------------------------------------------------------------------
# --------------------     Initial clean-up     --------------------
# ------------------------------------------------------------------

kill $(ps aux | grep ipfs | grep -v grep | awk '{print $2}')
rm -f .ipfs/repo.lock
rm -f common/CL_ID

# ------------------------------------------------------------------
# --------------------    Start IPFS service    --------------------
# ------------------------------------------------------------------

ipfs init 
ipfs version

export MY_IP=`hostname -i`
export MY_ID=`ipfs id --format="<id>"`
export MASTER_NODE_IP=`getent hosts ${MASTER_NODE}  | awk '{ print $1 }'`

#sleep 2
echo .
ipfs bootstrap rm --all
echo .
ipfs bootstrap add /ip4/${MASTER_NODE_IP}/tcp/4001/ipfs/${MY_ID}
echo .

sed -i -e $( echo "s/127.0.0.1/${MY_IP}/g" ) -e 's/10GB/15GB/g' .ipfs/config

sleep 2
#read -p "Press enter to continue -- Before: ipfs daemon &"
kill $(ps aux | grep ipfs | grep -v grep | awk '{print $2}')
rm -f .ipfs/repo.lock
sleep 2
echo .
ipfs daemon > /dev/null &

sleep 2
echo .
#read -p "Press enter to continue -- Before: ipfs swarm peers"
ipfs swarm peers
echo .


# ------------------------------------------------------------------
# --------------------    Start IPFS-Cluster    --------------------
# ------------------------------------------------------------------

echo .
ipfs-cluster-service --version
ipfs-cluster-ctl --version
echo .

# ---------------------------
# AT MASTER NODE ONLY!!!!

sleep 2
#read -p "Press enter to continue -- Before: ipfs-cluster-service init"
ipfs-cluster-service init

# EDIT .ipfs-cluster/service.json
sed -i "s/127.0.0.1\/tcp\/\(9095\|9097\|5001\)/${MY_IP}\/tcp\/\1/g" .ipfs-cluster/service.json

echo .
sleep 2
#read -p "Press enter to continue -- Before: ipfs-cluster-service daemon &"
ipfs-cluster-service daemon > /dev/null &
echo .

sleep 2
export CL_ID=`ipfs-cluster-ctl id | head -1 | cut -d ' ' -f 1`
echo .
echo !!!! CLUSTER MASTER ID !!!!
echo $CL_ID
echo "$CL_ID" &> common/CL_ID
chmod og-rwx common/CL_ID

echo .
echo .
echo .
ipfs-cluster-ctl peers ls
echo .
echo .
echo MASTER IS READY -- Check for ERRORS!!
