#!/usr/bin/bash

file=common/files/$1MiB-%d.bin
N=5

rm -rf OUT/*
mkdir -p OUT/

results_file=common/RESULTS-$1-`date +%s`.csv

# IPFS ADDs
echo "IPFS ADDs..."
for ((iter=1; iter<=$N; iter++))
do
  filename=`printf "$file\n" $iter`
  echo "Running iteration $iter  --  $filename"
  #echo $filename
  startTm=`date +%s%3N`
  cid=`ipfs add -Q $filename`
  endTm=`date +%s%3N`
  file_cids+=($cid)
  echo "$cid -- Dur: $(( $endTm - $startTm ))ms"
  echo "$1;ADD;$iter;$cid;$(( $endTm - $startTm ))" >> $results_file
done
echo ${file_cids[@]}
echo .

