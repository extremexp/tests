
# ------------------------------------------------------------------
# --------------------     Initial clean-up     --------------------
# ------------------------------------------------------------------

kill $(ps aux | grep ipfs | grep -v grep | awk '{print $2}')
rm -f .ipfs/repo.lock

until [ -f common/CL_ID ]
do
	echo Waiting for 'common/CL_ID'...
     sleep 2
done

export CL_ID=$(<common/CL_ID)
echo $CL_ID

# ------------------------------------------------------------------
# --------------------    Start IPFS service    --------------------
# ------------------------------------------------------------------

ipfs init 
ipfs version

export MY_IP=`hostname -i`
export MY_ID=`ipfs id --format="<id>"`
export MASTER_NODE_IP=`getent hosts ${MASTER_NODE}  | awk '{ print $1 }'`

#sleep 2
echo .
ipfs bootstrap rm --all
echo .
ipfs bootstrap add /ip4/${MASTER_NODE_IP}/tcp/4001/ipfs/${MY_ID}
echo .

sed -i -e $( echo "s/127.0.0.1/${MY_IP}/g" ) -e 's/10GB/15GB/g' .ipfs/config

sleep 2
#read -p "Press enter to continue -- Before: ipfs daemon &"
kill $(ps aux | grep ipfs | grep -v grep | awk '{print $2}')
rm -f .ipfs/repo.lock
sleep 2
echo .
ipfs daemon > /dev/null &

sleep 2
echo .
#read -p "Press enter to continue -- Before: ipfs swarm peers"
ipfs swarm peers
echo .


# ------------------------------------------------------------------
# --------------------    Start IPFS-Cluster    --------------------
# ------------------------------------------------------------------

echo .
ipfs-cluster-service --version
ipfs-cluster-ctl --version
echo .

# ---------------------------
# AT OTHER NODES!!!!

sleep 2
#read -p "Press enter to continue -- Before: ipfs-cluster-service init"
ipfs-cluster-service init

# EDIT .ipfs-cluster/service.json
sed -i "s/127.0.0.1\/tcp\/\(9095\|9097\|5001\)/${MY_IP}\/tcp\/\1/g" .ipfs-cluster/service.json

echo .
sleep 2
#read -p "Press enter to continue -- Before: ipfs-cluster-service daemon &"
ipfs-cluster-service daemon --bootstrap /ip4/${MASTER_NODE_IP}/tcp/9096/p2p/${CL_ID} > /dev/null &
echo .

echo .
echo .
echo .
sleep 2
ipfs-cluster-ctl peers ls
echo .
echo .
echo NODE IS READY -- Check for ERRORS!!
