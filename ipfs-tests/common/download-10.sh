#!/usr/bin/bash

file=common/files/$1MiB-%d.bin
N=10

rm -rf OUT/*
mkdir -p OUT/

results_file=common/RESULTS-$1-`date +%s`.csv

IFS=' ' read -ra file_cids <<< "QmW2aof6tk5vGhCio4muqAqidezBKj6SLM16HQJ2soHvnB QmZDZLN6d7QtTieAC4b76SbCNoeHafPog6rgpR5xyCMA8N QmPStgdqb9bwRnQc17QdiCyMqPD5bidvNkujqvXdLYJEmY QmRVpe3iY4ey8ZbMdQehrJuveSkF6nbzaC3MWV4Zoqi2S1 QmdkEAcMsoXdbSvvhdMjMCXaT6QApj746pkpVmoW1UyoXc"

echo ${file_cids[@]}
echo .

sleep 5

# IPFS GETs
echo "IPFS GETs..."
#for ((iter=1; iter<=$N; iter++))
iter=1
for f_cid in "${file_cids[@]}"
do
  echo "Running iteration $iter  --  $f_cid"
  #echo $f_cid
  startTm=`date +%s%3N`
  cid=`ipfs get --output=OUT/ $f_cid`
  endTm=`date +%s%3N`
  echo "$f_cid -- Dur: $(( $endTm - $startTm ))ms"
  echo "$1;GET;$iter;$f_cid;$(( $endTm - $startTm ))" >> $results_file
  let 'iter+=1'
done
