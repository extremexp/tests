#!/usr/bin/bash

file=common/files/$1MiB-%d.bin
N=10

rm -rf OUT/*
mkdir -p OUT/

results_file=common/RESULTS-`date +%s`.csv

# IPFS ADDs
echo "IPFS ADDs..."
for ((iter=1; iter<=$N; iter++))
do
  filename=`printf "$file\n" $iter`
  echo "Running iteration $iter  --  $filename"
  #echo $filename
  startTm=`date +%s%3N`
  cid=`ipfs add -Q $filename`
  endTm=`date +%s%3N`
  file_cids+=($cid)
  echo "$cid -- Dur: $(( $endTm - $startTm ))ms"
  echo "$1;ADD;$iter;$cid;$(( $endTm - $startTm ))" >> $results_file
done
echo ${file_cids[@]}
echo .

sleep 5

# IPFS GETs
echo "IPFS GETs..."
#for ((iter=1; iter<=$N; iter++))
iter=1
for f_cid in "${file_cids[@]}"
do
  echo "Running iteration $iter  --  $f_cid"
  #echo $f_cid
  startTm=`date +%s%3N`
  cid=`ipfs get --output=OUT/ $f_cid`
  endTm=`date +%s%3N`
  echo "$f_cid -- Dur: $(( $endTm - $startTm ))ms"
  echo "$1;GET;$iter;$f_cid;$(( $endTm - $startTm ))" >> $results_file
  let 'iter+=1'
done
