
# ------------------------------------------------------------------
# --------------------    Start IPFS service    --------------------
# ------------------------------------------------------------------

ipfs init 
ipfs version

ipfs-swarm-key-gen &> ~/.ipfs/swarm.key

export MY_IP=`hostname -i`
export MY_ID=`ipfs id --format="<id>"`
#export MASTER_NODE=n1
export MASTER_NODE_IP=`getent hosts ${MASTER_NODE}  | awk '{ print $1 }'`

ipfs bootstrap rm --all
ipfs bootstrap add /ip4/${MASTER_NODE_IP}/tcp/4001/ipfs/${MY_ID}

#vi .ipfs/config
sed -i -e $( echo "s/127.0.0.1/${MY_IP}/g" ) -e 's/10GB/15GB/g' .ipfs/config

ipfs daemon &
#ipfs daemon > /dev/null &

ipfs swarm peers


mkdir test-files
echo hello IPFS &> file.txt
ipfs add file.txt
ipfs cat QmYDAmHsFu5oWsJU21esKnN4ZRyE8txJNvrZxvxXLymC2L

# ------------------------------------------------------------------
# --------------------  Deploying IPFS-Cluster  --------------------
# ------------------------------------------------------------------

ipfs-cluster-service --version
ipfs-cluster-ctl --version

# Do this in any Linux before running docker-compose.yml
export CLUSTER_SECRET=$(od -vN 32 -An -tx1 /dev/urandom | tr -d ' \n')
echo $CLUSTER_SECRET
# WRITE the CLUSTER_SECRET in '.env' file


# ---------------------------
# AT MASTER NODE ONLY!!!!

ipfs-cluster-service init

# EDIT .ipfs-cluster/service.json
sed -i "s/127.0.0.1\/tcp\/\(9095\|9097\|5001\)/${MY_IP}\/tcp\/\1/g" .ipfs-cluster/service.json

ipfs-cluster-service daemon &
#ipfs-cluster-service daemon > /dev/null &

???? Get CLUSTER_ID
12D3KooWA5ASj35aPqQ3GieVgL3DEkvA1cK3YBJEWRyAYRwuVgax

# ---------------------------
# AT OTHER NODES!!!!

# ??????????????????????
# export CLUSTER_ID=12D3KooWA5ASj35aPqQ3GieVgL3DEkvA1cK3YBJEWRyAYRwuVgax

ipfs-cluster-service init

# EDIT .ipfs-cluster/service.json
sed -i "s/127.0.0.1\/tcp\/\(9095\|9097\|5001\)/${MY_IP}\/tcp\/\1/g" .ipfs-cluster/service.json

ipfs-cluster-service daemon --bootstrap /ip4/${MASTER_IP}/tcp/9096/p2p/${CLUSTER_ID} &

ipfs-cluster-ctl peers ls
ipfs-cluster-ctl add myfile.txt 
ipfs-cluster-ctl status QmVx1Jt6jEMEYoKfpBCddgqoMF63jjQxQ6tEjWZJPbiUjc
