
BUILD DOCKER IMAGE
==================
git clone https://gitlab.com/extremexp/tests.git
cd tests/ipfs-tests
docker compose build


INIT SWARM and CLUSTER SECRET
=============================
docker compose up -d n1
docker exec -it n1 bash
ipfs-swarm-key-gen &> common/swarm.key

echo $(od -vN 32 -An -tx1 /dev/urandom | tr -d ' \n')
# ...and write the value to .env at P_CLUSTER_SECRET

exit
docker compose down



START CONTAINERS
================

docker compose up -d		# START all nodes
docker compose up -d n1		# START 1 node (named "n1")

docker exec -it n1 bash		# ...or n2, n3

# AT MASTER (n1)
./common/test-master.sh

# AT NODES (n2, n3)
./common/test-node.sh


RUN EXPERIMENTS
===============

# AT MASTER
./common/run-10.sh 1
./common/run-10.sh 10
./common/run-10.sh 100
./common/run-10.sh 1000


EXIT CONTAINERS
===============

# AT MASTER and NODES
exit

docker compose down

-------------------------------------------------------------------------
TEMPORARY NOTES on TESTS with "AWS VMs" in the SAME REGION
-------------------------------------------------------------------------

VM-A (master):
./common/test-master.sh
	--> !! SOS !! : this changes 'common/CL_ID'

VM-B (nodes):
set VM-A's CL_ID to 'common/CL_ID'
then....
./common/test-node-aws.sh


### USING JUST "ipfs"  (no cluster)
VM-A:
ipfs id
		--> GET "ID", e.g. 'QmUBJBJNfKU95R4Aj2nu2MfsDf2BwuWRpNczssCaxfespw'

VM-B:
ipfs swarm connect  /ip4/__VM_A_NODE_PUBLIC_IP_ADDRES__/tcp/4001/ipfs/__VM_A_IPFS_ID__
ipfs swarm peers
ipfs get <FILE_HASH>
ipfs ping /ip4/__VM_A_NODE_PUBLIC_IP_ADDRES__/tcp/4001/ipfs/__VM_A_IPFS_ID__


### USING "ipfs-cluster"
VM-A:
ipfs-cluster-ctl add TEST
ipfs-cluster-ctl pin ls

VM-B:
ipfs-cluster-ctl peers ls
ipfs-cluster-ctl pin ls
ipfs get <FILE_HASH>
