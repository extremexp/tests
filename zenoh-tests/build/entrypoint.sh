#!/usr/bin/bash

# Set shell encoding to UTF-8 (in order to display banner correctly)
export LANG=C.UTF-8

# Setup log file
if [ -z "$ZENOH_LOG_FILE" ]; then ZENOH_LOG_FILE=/var/log/zenohd.log; fi
touch $ZENOH_LOG_FILE
echo "-------------------------------------------------------------------------------" >> $ZENOH_LOG_FILE

# Start Zenoh router
if [ -f "$ZENOH_CONF_FILE" ]; then
  echo "Starting Zenoh Router with configuration file: ${ZENOH_CONF_FILE}...."
  zenohd -c $ZENOH_CONF_FILE |& tee -a $ZENOH_LOG_FILE &
else
  echo "Starting Zenoh Router with default configuration...."
  zenohd |& tee -a $ZENOH_LOG_FILE &
fi
pid=$!

# Setup TERM & INT signal handler
trap 'echo "Signaled Zenoh Router to exit"; kill -TERM "${pid}"; wait "${pid}"; ' SIGTERM SIGINT SIGKILL
#echo "Pid: $pid"
wait ${pid}